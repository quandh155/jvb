DROP TRIGGER IF EXISTS `syslog_ins_trigger`;
DELIMITER $$
CREATE TRIGGER `syslog_ins_trigger` AFTER INSERT ON `eventlog` FOR EACH ROW begin 
DECLARE IP varchar(255) DEFAULT '0';
DECLARE NodeID varchar(255) Default '';
DECLARE var1 varchar(255) Default '';
DECLARE var2 varchar(255) Default '';
DECLARE Content_ID varchar(255) Default '';
DECLARE var3 varchar(255) Default '';
DECLARE S varchar(255) Default '';
DECLARE C varchar(255) Default '';
DECLARE D varchar(255) Default '';
DECLARE id_of_node varchar(255) Default NULL;
DECLARE var4 varchar(255) Default '';
DECLARE check_port varchar(255) Default '';
DECLARE noname varchar(255) Default '';
DECLARE prior INT Default '';
DECLARE beforeAlivecondition varchar(255) Default '';
DECLARE afterAlivecondition varchar(255) Default '';
DECLARE last_id INT DEFAULT '';
DECLARE var5 INT DEFAULT '';
DECLARE var6 INT DEFAULT '10';
DECLARE gr_content_id INT DEFAULT '';
DECLARE gr_content_interval INT DEFAULT '10';
DECLARE Content_Name text Default '';
DECLARE Client_Name text Default '';

SET IP = ( SELECT label from nodetable Where address = new.agent_addr and node_id = new.map_id);

-- check node
if exists ( SELECT node_id from nodetable Where address = new.agent_addr and node_id = new.map_id)
  Then
  SET NodeID  = ( SELECT node_id from nodetable Where address = new.agent_addr and node_id = new.map_id);
  SET beforeAlivecondition = ( Select `tra_nodes`.`alive_condition` from `tra_nodes` LEFT JOIN `nodes` ON `nodes`.`id` = `tra_nodes`.`nodes_id` Where `nodes`.`node_id` = NodeID ORDER BY `nodes`.`id` ASC limit 1);
  set noname = ( Select nodetable.label from nodetable Where nodetable.address = new.agent_addr and nodetable.node_id = new.map_id);
  if(new.message like ('%Device responding to polling requests%'))
    Then
    set afterAlivecondition = 1;
  elseif(new.message like ('%No response to device polling request, including retries%')) 
    Then
    set afterAlivecondition = 0;
  elseif(new.message == like('%重複するソフトウェアキーが検出されました。%'))
    then
    set print_f('NO INSERT');
  end if;
else 
  set noname = new.agent_addr;
end if;

-- end check node

-- check message
IF (((new.from_addr = '192.168.11.34' OR new.from_addr = '127.0.0.1') and ( new.message like('%Object Added%') OR new.message like('%Object Changed%') OR new.message like('%Object Deleted%') OR new.message like('%User Administrator%')) ) OR  ( new.message like('%alarmOnEventByAlarmStop%') OR new.message like('%alarmOffEventByAlarmStop%') OR new.message like('%coldStart%')  OR new.message like('%ALARM STOP button released%')) OR new.message like('%Connectivity Loss (No response to multiple requests, including retries)%') OR (beforeAlivecondition like ('%1%') and afterAlivecondition like ('%1%')) OR new.message like('%SnmpcServer%') OR new.message like('%HistoryPoller%') OR new.message like('%ディスカバリ/ステータスエージェントがサーバに接続されています%') OR new.message like('%トレンドレポートエージェントがサーバに接続されています%') OR new.message like('%Backup Service Enabled%') OR new.message like('%NodeSetup Info:%') OR (beforeAlivecondition like ('%0%') and afterAlivecondition like ('%0%')) OR new.message like('%ディスカバリ/ステータスエージェントがサーバに接続されていません%'))
  THEN
  SET var3 = 1;
ELSEIF(new.message like('%ALARM STOP button pushed%'))
  then
  update exclusion_trigger_serverjs set locked = 1;
  update syslog set pato_valid = 0 where pato_valid = 1;
  update exclusion_trigger_serverjs set locked = 0;
ELSEIF(new.message  NOT like('重複するソフトウェアキーが検出されました。'))
then
set NULL;
ELSE
  If (IP like ('%MD003%') OR IP like ('%MD-003%'))
    THEN
    SET id_of_node  = ( SELECT id from nodes Where node_id = NodeID and slot = SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' ',-2),' ',1) and unit_name = SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' ',-1),' ',-1) ORDER BY `nodes`.`id` ASC limit 1);  
    
    SET var1  = ( SELECT place from nodes Where node_id = NodeID and slot = SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' ',-2),' ',1) and unit_name = SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' ',-1),' ',-1) ORDER BY `nodes`.`id` ASC limit 1);
    SET var2 = ( SELECT device_name from nodes Where node_id = NodeID and slot = SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' ',-2),' ',1) and unit_name = SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' ',-1),' ',-1) ORDER BY `nodes`.`id` ASC limit 1);
    SET Content_Name  = (SELECT GROUP_CONCAT(DISTINCT `name` SEPARATOR '、') listing FROM
                      ( 
                        SELECT C.`name`
                        FROM `nodes_to_contents` N LEFT JOIN `contents` C ON C.`id` = N.`contents_id`  
                        Where N.`node_id` = id_of_node ) AA);

    SET Client_Name  = (SELECT CONCAT_WS('、',`to_client`,`name`) listing FROM
                      ( 
                        SELECT A.`name`, GROUP_CONCAT(B.`name` SEPARATOR '、') `to_client`
                        FROM `nodes_to_contents` N 
                        LEFT JOIN `contents` C ON C.`id` = N.`contents_id` 
                        LEFT JOIN `clients` A ON C.`contract_clients_id` = A.`id` 
                        LEFT JOIN `contents_to_client` D ON C.`id` = D.`contents_id` 
                        LEFT JOIN `clients` B ON D.`to_client_id` = B.`id` 
                        Where N.`node_id` = id_of_node ) AA);
    
    SET var4 = ( SELECT inhibit from nodes Where node_id = NodeID and slot = SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' ',-2),' ',1) and unit_name = SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' ',-1),' ',-1) ORDER BY `nodes`.`id` ASC limit 1);

    -- check group parent 
    SET var6 = ( SELECT grouping_interval from nodes where id = id_of_node ORDER BY id DESC limit 1);
    If (var6 IS NULL)
      THEN
      SET var6 = 10;
    end if;
    SET var5 = ( SELECT grouping_parent_id from syslog where node_name = noname AND slot = SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' ',-2),' ',1) AND unit = SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' ',-1),' ',-1) AND exec_time >= DATE_SUB(new.date_time, INTERVAL var6 SECOND) ORDER BY `syslog`.`id` DESC limit 1);

    -- check group line number
    SET gr_content_id = ( SELECT grouping_content_id from syslog where content = Content_Name AND exec_time >= DATE_SUB(new.date_time, INTERVAL var6 SECOND) ORDER BY `syslog`.`id` DESC limit 1);

    if(var4 like '%1%')
      then
      SET var3 = 1;
    elseIf ((new.message like ('%No response to device polling request, including retries%')) or (new.message like ('%Device responding to polling requests%')))
      then
      SET var1  = ( SELECT GROUP_CONCAT(DISTINCT `place` SEPARATOR '、') from `nodes` Where `node_id` = NodeID);
      SET var2 = ( SELECT GROUP_CONCAT(DISTINCT `device_name` SEPARATOR '、') from `nodes` Where `node_id` = NodeID);
      SET Content_Name  = ( 
                          SELECT GROUP_CONCAT(DISTINCT C.`name` SEPARATOR '、') 
                          FROM `nodes` B 
                          LEFT JOIN `nodes_to_contents` N  ON B.`id` = N.`node_id`
                          LEFT JOIN `contents` C ON C.`id` = N.`contents_id`
                          Where B.`node_id` = NodeID );
      SET Client_Name  = (SELECT CONCAT_WS('、',`to_client`,`name`) listing FROM
                        ( 
                          SELECT GROUP_CONCAT(DISTINCT A.`name` SEPARATOR '、') `name`, GROUP_CONCAT(DISTINCT B.`name` SEPARATOR '、') `to_client`
                          FROM `nodes` N1 
                          LEFT JOIN `nodes_to_contents` N ON N1.`id` = N.`node_id` 
                          LEFT JOIN `contents` C ON C.`id` = N.`contents_id` 
                          LEFT JOIN `clients` A ON C.`contract_clients_id` = A.`id` 
                          LEFT JOIN `contents_to_client` D ON C.`id` = D.`contents_id` 
                          LEFT JOIN `clients` B ON D.`to_client_id` = B.`id`  
                          Where N1.`node_id` = NodeID ) AA);
      -- insert
      insert into syslog (log, priority, exec_time, create_time, update_time, event_id, map_id, ip_addr,status, node_name, place, device_name, node_id, content, client_name)
        values (SUBSTRING_INDEX(new.message,' - ',-1), new.priority, new.date_time, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, new.event_id, new.map_id, new.agent_addr,'1', noname, var1, var2, id_of_node, Content_Name, Client_Name);
      SET last_id = ( SELECT LAST_INSERT_ID());
      -- update group parent and group line number
      IF (var5 and ( SELECT exec_time from syslog where grouping_parent_id = var5 ORDER BY `syslog`.`id` ASC limit 1) >= DATE_SUB(new.date_time, INTERVAL var6 SECOND))
        Then
        UPDATE syslog SET grouping_parent_id = last_id where grouping_parent_id = var5 OR id = last_id;
      else
        UPDATE syslog SET grouping_parent_id = last_id where id = last_id;
      end if;
      IF ( (Content_Name IS NULL) OR (( SELECT exec_time from syslog where grouping_content_id = gr_content_id ORDER BY `syslog`.`id` ASC limit 1) < DATE_SUB(new.date_time, INTERVAL var6 SECOND)))
        Then
        UPDATE syslog SET grouping_content_id = last_id where id = last_id;
      else
        UPDATE syslog SET grouping_content_id = last_id where grouping_content_id = gr_content_id OR id = last_id;
      end if;
    else
      -- insert
      insert into syslog (log, priority, exec_time, create_time, update_time, event_id, map_id, ip_addr,status, slot, node_name, unit, place, device_name, node_id, content, client_name)
      values (SUBSTRING_INDEX(SUBSTRING_INDEX(new.message,' - ',-1),' Slot ',1), new.priority, new.date_time, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, new.event_id, new.map_id, new.agent_addr,'1', SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' ',-2),' ',1), noname, SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' ',-1),' ',-1), var1, var2, id_of_node, Content_Name, Client_Name);
      SET last_id = ( SELECT LAST_INSERT_ID());
      -- update group parent and group line number
      IF (var5 and ( SELECT exec_time from syslog where grouping_parent_id = var5 ORDER BY `syslog`.`id` ASC limit 1) >= DATE_SUB(new.date_time, INTERVAL var6 SECOND))
        Then
        UPDATE syslog SET grouping_parent_id = last_id where grouping_parent_id = var5 OR id = last_id;
      else
        UPDATE syslog SET grouping_parent_id = last_id where id = last_id;
      end if;
      IF ( (Content_Name IS NULL) OR (( SELECT exec_time from syslog where grouping_content_id = gr_content_id ORDER BY `syslog`.`id` ASC limit 1) < DATE_SUB(new.date_time, INTERVAL var6 SECOND)))
        Then
        UPDATE syslog SET grouping_content_id = last_id where id = last_id;
      else
        UPDATE syslog SET grouping_content_id = last_id where grouping_content_id = gr_content_id OR id = last_id;
      end if;
    end if;
  elseIf (IP like '%MD8000%')
    Then
    if(new.message like ('%md8000TrapVar%'))
      then
      SET id_of_node  = ( SELECT id from nodes Where node_id = NodeID AND slot = SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),'[3]',1),': ',-1) AND unit_name = SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),'[4]',1),': ',-1) ORDER BY `nodes`.`id` ASC limit 1);
      
      SET var1  = ( SELECT place from nodes Where node_id = NodeID AND slot = SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),'[3]',1),': ',-1) AND unit_name = SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),'[4]',1),': ',-1) ORDER BY `nodes`.`id` ASC limit 1);
      SET var2 = ( SELECT device_name from nodes Where node_id = NodeID AND slot = SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),'[3]',1),': ',-1) AND unit_name = SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),'[4]',1),': ',-1) ORDER BY `nodes`.`id` ASC limit 1);
      SET Content_Name  = (SELECT GROUP_CONCAT(DISTINCT `name` SEPARATOR '、') listing FROM
                      ( 
                        SELECT C.`name`
                        FROM `nodes_to_contents` N LEFT JOIN `contents` C ON C.`id` = N.`contents_id`  
                        Where N.`node_id` = id_of_node ) AA);

      SET Client_Name  = (SELECT CONCAT_WS('、',`to_client`,`name`) listing FROM
                      ( 
                        SELECT A.`name`, GROUP_CONCAT(B.`name` SEPARATOR '、') `to_client`
                        FROM `nodes_to_contents` N 
                        LEFT JOIN `contents` C ON C.`id` = N.`contents_id` 
                        LEFT JOIN `clients` A ON C.`contract_clients_id` = A.`id` 
                        LEFT JOIN `contents_to_client` D ON C.`id` = D.`contents_id` 
                        LEFT JOIN `clients` B ON D.`to_client_id` = B.`id` 
                        Where N.`node_id` = id_of_node ) AA);
      
      insert into syslog (log, priority, exec_time, create_time, update_time, event_id, map_id, ip_addr,status, slot, node_name, unit, place, device_name, content, client_name, node_id)
      values (new.message, new.priority, new.date_time, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, new.event_id, new.map_id, new.agent_addr,'1', SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),'[3]',1),': ',-1), noname, SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),'[4]',1),': ',-1), var1, var2, Content_Name, Client_Name, id_of_node );
      SET last_id = ( SELECT LAST_INSERT_ID());
      UPDATE syslog SET grouping_parent_id = last_id where id = last_id;
      UPDATE syslog SET grouping_content_id = last_id where id = last_id;
    elseIf ((new.message like ('%No response to device polling request, including retries%')) or (new.message like ('%Device responding to polling requests%')))
      then
      SET var1  = ( SELECT GROUP_CONCAT(DISTINCT `place` SEPARATOR '、') from `nodes` Where `node_id` = NodeID);
      SET var2 = ( SELECT GROUP_CONCAT(DISTINCT `device_name` SEPARATOR '、') from `nodes` Where `node_id` = NodeID);
      SET Content_Name  = ( 
                          SELECT GROUP_CONCAT(DISTINCT C.`name` SEPARATOR '、') 
                          FROM `nodes` B 
                          LEFT JOIN `nodes_to_contents` N  ON B.`id` = N.`node_id`
                          LEFT JOIN `contents` C ON C.`id` = N.`contents_id`
                          Where B.`node_id` = NodeID );
      SET Client_Name  = (SELECT CONCAT_WS('、',`to_client`,`name`) listing FROM
                        ( 
                          SELECT GROUP_CONCAT(DISTINCT A.`name` SEPARATOR '、') `name`, GROUP_CONCAT(DISTINCT B.`name` SEPARATOR '、') `to_client`
                          FROM `nodes` N1 
                          LEFT JOIN `nodes_to_contents` N ON N1.`id` = N.`node_id` 
                          LEFT JOIN `contents` C ON C.`id` = N.`contents_id` 
                          LEFT JOIN `clients` A ON C.`contract_clients_id` = A.`id` 
                          LEFT JOIN `contents_to_client` D ON C.`id` = D.`contents_id` 
                          LEFT JOIN `clients` B ON D.`to_client_id` = B.`id`  
                          Where N1.`node_id` = NodeID ) AA);
      insert into syslog (log, priority, exec_time, create_time, update_time, event_id, map_id, ip_addr,status, node_name, place, device_name, node_id, content, client_name)
      values (SUBSTRING_INDEX(new.message,' - ',-1), new.priority, new.date_time, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, new.event_id, new.map_id, new.agent_addr,'1', noname, var1, var2, id_of_node, Content_Name, Client_Name);
      SET last_id = ( SELECT LAST_INSERT_ID());
      UPDATE syslog SET grouping_parent_id = last_id where id = last_id;
      UPDATE syslog SET grouping_content_id = last_id where id = last_id;
    elseIf(new.message like ('%Service%'))
      then
      SET id_of_node  = ( SELECT id from nodes Where node_id = NodeID AND slot = SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' Slot ',-1),' ',1) AND port = SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' Service ',-1),' ',1) ORDER BY `nodes`.`id` ASC limit 1);
      
      SET var1  = ( SELECT place from nodes Where node_id = NodeID AND slot = SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' Slot ',-1),' ',1) AND port = SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' Service ',-1),' ',1) ORDER BY `nodes`.`id` ASC limit 1);
      SET var2 = ( SELECT device_name from nodes Where node_id = NodeID AND slot = SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' Slot ',-1),' ',1) AND port = SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' Service ',-1),' ',1) ORDER BY `nodes`.`id` ASC limit 1);
      SET Content_Name  = (SELECT GROUP_CONCAT(DISTINCT `name` SEPARATOR '、') listing FROM
                      ( 
                        SELECT C.`name`
                        FROM `nodes_to_contents` N LEFT JOIN `contents` C ON C.`id` = N.`contents_id`  
                        Where N.`node_id` = id_of_node ) AA);

      SET Client_Name  = (SELECT CONCAT_WS('、',`to_client`,`name`) listing FROM
                      ( 
                        SELECT A.`name`, GROUP_CONCAT(B.`name` SEPARATOR '、') `to_client`
                        FROM `nodes_to_contents` N 
                        LEFT JOIN `contents` C ON C.`id` = N.`contents_id` 
                        LEFT JOIN `clients` A ON C.`contract_clients_id` = A.`id` 
                        LEFT JOIN `contents_to_client` D ON C.`id` = D.`contents_id` 
                        LEFT JOIN `clients` B ON D.`to_client_id` = B.`id` 
                        Where N.`node_id` = id_of_node ) AA);
      SET var4  = ( SELECT inhibit from nodes Where node_id = NodeID and slot = SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' Slot ',-1),' ',1) and port = SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' Service ',-1),' ',1) ORDER BY `nodes`.`id` ASC limit 1);

      -- check group parent 
      SET var6 = ( SELECT grouping_interval from nodes where id = id_of_node ORDER BY id DESC limit 1);
      If (var6 IS NULL)
        THEN
        SET var6 = 10;
      end if;
      SET var5 = ( SELECT grouping_parent_id from syslog where node_name = noname AND slot = SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' Slot ',-1),' ',1) AND port = SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' Service ',-1),' ',1) AND exec_time >= DATE_SUB(new.date_time, INTERVAL var6 SECOND) ORDER BY `syslog`.`id` DESC limit 1);
      
      -- check group line number
      SET gr_content_id = ( SELECT grouping_content_id from syslog where content = Content_Name AND exec_time >= DATE_SUB(new.date_time, INTERVAL var6 SECOND) ORDER BY `syslog`.`id` DESC limit 1);

      if(var4 like '%1%')
        then
        SET var3 = 1;
      else
        -- insert
        insert into syslog (log, priority, exec_time, create_time, update_time, event_id, map_id, ip_addr,status, slot, node_name, port,unit, place, device_name, node_id, content, client_name )
        values (SUBSTRING_INDEX(SUBSTRING_INDEX(new.message,' - ',-1),'Slot',1), new.priority, new.date_time, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, new.event_id, new.map_id, new.agent_addr,'1', SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' Slot ',-1),' ',1), noname, SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' Service ',-1),' ',1),SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' ',-1),' ',1), var1, var2, id_of_node, Content_Name, Client_Name);
        SET last_id = ( SELECT LAST_INSERT_ID());
        -- update group parent and group line number
        IF (var5 and ( SELECT exec_time from syslog where grouping_parent_id = var5 ORDER BY `syslog`.`id` ASC limit 1) >= DATE_SUB(new.date_time, INTERVAL var6 SECOND))
          Then
          UPDATE syslog SET grouping_parent_id = last_id where grouping_parent_id = var5 OR id = last_id;
        else
          UPDATE syslog SET grouping_parent_id = last_id where id = last_id;
        end if;
        IF ( (Content_Name IS NULL) OR (( SELECT exec_time from syslog where grouping_content_id = gr_content_id ORDER BY `syslog`.`id` ASC limit 1) < DATE_SUB(new.date_time, INTERVAL var6 SECOND)))
          Then
          UPDATE syslog SET grouping_content_id = last_id where id = last_id;
        else
          UPDATE syslog SET grouping_content_id = last_id where grouping_content_id = gr_content_id OR id = last_id;
        end if;
      end if;
    elseIf(new.message like ('%Port%'))
      then
      SET id_of_node  = ( SELECT id from nodes Where node_id = NodeID AND slot = SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' Slot ',-1),' ',1) AND port = SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' Port ',-1),' ',1) ORDER BY `nodes`.`id` ASC limit 1);
      
      SET var1  = ( SELECT place from nodes Where node_id = NodeID AND slot = SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' Slot ',-1),' ',1) and port = SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' Port ',-1),' ',1) ORDER BY `nodes`.`id` ASC limit 1);
      SET var2 = ( SELECT device_name from nodes Where node_id = NodeID AND slot = SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' Slot ',-1),' ',1) and port = SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' Port ',-1),' ',1) ORDER BY `nodes`.`id` ASC limit 1);
      SET Content_Name  = (SELECT GROUP_CONCAT(DISTINCT `name` SEPARATOR '、') listing FROM
                      ( 
                        SELECT C.`name`
                        FROM `nodes_to_contents` N LEFT JOIN `contents` C ON C.`id` = N.`contents_id`  
                        Where N.`node_id` = id_of_node ) AA);

      SET Client_Name  = (SELECT CONCAT_WS('、',`to_client`,`name`) listing FROM
                      ( 
                        SELECT A.`name`, GROUP_CONCAT(B.`name` SEPARATOR '、') `to_client`
                        FROM `nodes_to_contents` N 
                        LEFT JOIN `contents` C ON C.`id` = N.`contents_id` 
                        LEFT JOIN `clients` A ON C.`contract_clients_id` = A.`id` 
                        LEFT JOIN `contents_to_client` D ON C.`id` = D.`contents_id` 
                        LEFT JOIN `clients` B ON D.`to_client_id` = B.`id` 
                        Where N.`node_id` = id_of_node ) AA);
      SET var4  = ( SELECT inhibit from nodes Where node_id = NodeID and slot = SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' Slot ',-1),' ',1) and port = SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' Port ',-1),' ',1) ORDER BY `nodes`.`id` ASC limit 1);

      -- check group parent 
      SET var6 = ( SELECT grouping_interval from nodes where id = id_of_node ORDER BY id DESC limit 1);
      If (var6 IS NULL)
        THEN
        SET var6 = 10;
      end if;
      SET var5 = ( SELECT grouping_parent_id from syslog where node_name = noname AND slot = SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' Slot ',-1),' ',1) AND port = SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' Port ',-1),' ',1) AND exec_time >= DATE_SUB(new.date_time, INTERVAL var6 SECOND) ORDER BY `syslog`.`id` DESC limit 1);
      
      -- check group line number
      SET gr_content_id = ( SELECT grouping_content_id from syslog where content = Content_Name AND exec_time >= DATE_SUB(new.date_time, INTERVAL var6 SECOND) ORDER BY `syslog`.`id` DESC limit 1);

      if(var4 like '%1%')
        then
        SET var3 = 1;
      else
        -- insert
        insert into syslog (log, priority, exec_time, create_time, update_time, event_id, map_id, ip_addr,status, slot, node_name, port,unit, place, device_name, node_id, content, client_name )
        values (SUBSTRING_INDEX(SUBSTRING_INDEX(new.message,' - ',-1),'Slot',1), new.priority, new.date_time, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, new.event_id, new.map_id, new.agent_addr,'1', SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' Slot ',-1),' ',1), noname, SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' Port ',-1),' ',1),SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' ',-1),' ',1), var1, var2, id_of_node, Content_Name, Client_Name);
        SET last_id = ( SELECT LAST_INSERT_ID());
        -- update group parent and group line number
        IF (var5 and ( SELECT exec_time from syslog where grouping_parent_id = var5 ORDER BY `syslog`.`id` ASC limit 1) >= DATE_SUB(new.date_time, INTERVAL var6 SECOND))
          Then
          UPDATE syslog SET grouping_parent_id = last_id where grouping_parent_id = var5 OR id = last_id;
        else
          UPDATE syslog SET grouping_parent_id = last_id where id = last_id;
        end if;
        IF ( (Content_Name IS NULL) OR (( SELECT exec_time from syslog where grouping_content_id = gr_content_id ORDER BY `syslog`.`id` ASC limit 1) < DATE_SUB(new.date_time, INTERVAL var6 SECOND)))
          Then
          UPDATE syslog SET grouping_content_id = last_id where id = last_id;
        else
          UPDATE syslog SET grouping_content_id = last_id where grouping_content_id = gr_content_id OR id = last_id;
        end if;
      end if;
    else  
      SET id_of_node  = ( SELECT id from nodes Where node_id = NodeID AND slot = SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' Slot ',-1),' ',1) ORDER BY `nodes`.`id` ASC limit 1);
      
      SET var1  = ( SELECT place from nodes Where node_id = NodeID AND slot = SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' Slot ',-1),' ',1) ORDER BY `nodes`.`id` ASC limit 1);
      SET var2 = ( SELECT device_name from nodes Where node_id = NodeID AND slot = SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' Slot ',-1),' ',1) ORDER BY `nodes`.`id` ASC limit 1);
      SET Content_Name  = (SELECT GROUP_CONCAT(DISTINCT `name` SEPARATOR '、') listing FROM
                      ( 
                        SELECT C.`name`
                        FROM `nodes_to_contents` N LEFT JOIN `contents` C ON C.`id` = N.`contents_id`  
                        Where N.`node_id` = id_of_node ) AA);

      SET Client_Name  = (SELECT CONCAT_WS('、',`to_client`,`name`) listing FROM
                      ( 
                        SELECT A.`name`, GROUP_CONCAT(B.`name` SEPARATOR '、') `to_client`
                        FROM `nodes_to_contents` N 
                        LEFT JOIN `contents` C ON C.`id` = N.`contents_id` 
                        LEFT JOIN `clients` A ON C.`contract_clients_id` = A.`id` 
                        LEFT JOIN `contents_to_client` D ON C.`id` = D.`contents_id` 
                        LEFT JOIN `clients` B ON D.`to_client_id` = B.`id` 
                        Where N.`node_id` = id_of_node ) AA);
      SET var4  = ( SELECT inhibit from nodes Where node_id = NodeID and slot = SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' Slot ',-1),' ',1) ORDER BY `nodes`.`id` ASC limit 1);

      -- check group parent 
      SET var6 = ( SELECT grouping_interval from nodes where id = id_of_node ORDER BY id DESC limit 1);
      If (var6 IS NULL)
        THEN
        SET var6 = 10;
      end if;
      SET var5 = ( SELECT grouping_parent_id from syslog where node_name = noname AND slot = SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' Slot ',-1),' ',1) AND exec_time >= DATE_SUB(new.date_time, INTERVAL var6 SECOND) ORDER BY `syslog`.`id` DESC limit 1);
      -- check group line number
      SET gr_content_id = ( SELECT grouping_content_id from syslog where content = Content_Name AND exec_time >= DATE_SUB(new.date_time, INTERVAL var6 SECOND) ORDER BY `syslog`.`id` DESC limit 1);

      if(var4 like '%1%')
        then
        SET var3 = 1;
      else
        -- insert
        insert into syslog (log, priority, exec_time, create_time, update_time, event_id, map_id, ip_addr,status, slot, node_name, unit, place, device_name, node_id, content, client_name )
        values (SUBSTRING_INDEX(SUBSTRING_INDEX(new.message,' - ',-1),'Slot',1), new.priority, new.date_time, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, new.event_id, new.map_id, new.agent_addr,'1', SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' Slot ',-1),' ',1), noname, SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' ',-1),' ',1), var1, var2, id_of_node, Content_Name, Client_Name);
        SET last_id = ( SELECT LAST_INSERT_ID());
        -- update group parent and group line number
        IF (var5 and ( SELECT exec_time from syslog where grouping_parent_id = var5 ORDER BY `syslog`.`id` ASC limit 1) >= DATE_SUB(new.date_time, INTERVAL var6 SECOND))
          Then
          UPDATE syslog SET grouping_parent_id = last_id where grouping_parent_id = var5 OR id = last_id;
        else
          UPDATE syslog SET grouping_parent_id = last_id where id = last_id;
        end if;
        IF ( (Content_Name IS NULL) OR (( SELECT exec_time from syslog where grouping_content_id = gr_content_id ORDER BY `syslog`.`id` ASC limit 1) < DATE_SUB(new.date_time, INTERVAL var6 SECOND)))
          Then
          UPDATE syslog SET grouping_content_id = last_id where id = last_id;
        else
          UPDATE syslog SET grouping_content_id = last_id where grouping_content_id = gr_content_id OR id = last_id;
        end if;
      end if;
    end if;
  elseIf (IP like ('%7%') AND (IP like ('%FR%') or IP like ('%FC%')))
    Then
    If(new.message like ('% |Port %'))
      then
      if (SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' |Slot ',-1),' ',1) != 1) 
        then
        SET id_of_node  = ( SELECT id from nodes Where node_id = NodeID AND slot = SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' |Slot ',-1),' ',1) AND port = SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' |Port ',-1),' ',1) ORDER BY `nodes`.`id` ASC limit 1);
        SET var1  = ( SELECT place from nodes Where node_id = NodeID AND slot = SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' |Slot ',-1),' ',1) AND port = SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' |Port ',-1),' ',1) ORDER BY `nodes`.`id` ASC limit 1);
        SET var2 = ( SELECT device_name from nodes Where node_id = NodeID AND slot = SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' |Slot ',-1),' ',1) AND port = SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' |Port ',-1),' ',1) ORDER BY `nodes`.`id` ASC limit 1);
        SET Content_Name  = (SELECT GROUP_CONCAT(DISTINCT `name` SEPARATOR '、') listing FROM
                      ( 
                        SELECT C.`name`
                        FROM `nodes_to_contents` N LEFT JOIN `contents` C ON C.`id` = N.`contents_id`  
                        Where N.`node_id` = id_of_node ) AA);

        SET Client_Name  = (SELECT CONCAT_WS('、',`to_client`,`name`) listing FROM
                      ( 
                        SELECT A.`name`, GROUP_CONCAT(B.`name` SEPARATOR '、') `to_client`
                        FROM `nodes_to_contents` N 
                        LEFT JOIN `contents` C ON C.`id` = N.`contents_id` 
                        LEFT JOIN `clients` A ON C.`contract_clients_id` = A.`id` 
                        LEFT JOIN `contents_to_client` D ON C.`id` = D.`contents_id` 
                        LEFT JOIN `clients` B ON D.`to_client_id` = B.`id` 
                        Where N.`node_id` = id_of_node ) AA);
        SET var4  = (Select inhibit from nodes where node_id = NodeID and slot = SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' |Slot ',-1),' ',1) ORDER BY `nodes`.`id` ASC limit 1 );
      
      else
        SET id_of_node  = ( SELECT id from nodes Where node_id = NodeID ORDER BY `nodes`.`id` ASC limit 1);
        SET var1  = ( SELECT place from nodes Where node_id = NodeID AND slot = SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' |Slot ',-1),' ',1) ORDER BY `nodes`.`id` ASC limit 1);
        SET var2 = ( SELECT device_name from nodes Where node_id = NodeID AND slot = SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' |Slot ',-1),' ',1) ORDER BY `nodes`.`id` ASC limit 1);
        SET Content_Name  = ( 
                          SELECT GROUP_CONCAT(DISTINCT C.`name` SEPARATOR '、') 
                          FROM `nodes` B 
                          LEFT JOIN `nodes_to_contents` N  ON B.`id` = N.`node_id`
                          LEFT JOIN `contents` C ON C.`id` = N.`contents_id`
                          Where B.`node_id` = NodeID );
        SET Client_Name  = (SELECT CONCAT_WS('、',`to_client`,`name`) listing FROM
                        ( 
                          SELECT GROUP_CONCAT(DISTINCT A.`name` SEPARATOR '、') `name`, GROUP_CONCAT(DISTINCT B.`name` SEPARATOR '、') `to_client`
                          FROM `nodes` N1 
                          LEFT JOIN `nodes_to_contents` N ON N1.`id` = N.`node_id` 
                          LEFT JOIN `contents` C ON C.`id` = N.`contents_id` 
                          LEFT JOIN `clients` A ON C.`contract_clients_id` = A.`id` 
                          LEFT JOIN `contents_to_client` D ON C.`id` = D.`contents_id` 
                          LEFT JOIN `clients` B ON D.`to_client_id` = B.`id`  
                          Where N1.`node_id` = NodeID ) AA);
        SET var4  = (Select inhibit from nodes where node_id = NodeID and slot = SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' |Slot ',-1),' ',1) ORDER BY `nodes`.`id` ASC limit 1 );
      end if;

      -- check group parent 
      SET var6 = ( SELECT grouping_interval from nodes where id = id_of_node ORDER BY id DESC limit 1);
      If (var6 IS NULL)
        THEN
        SET var6 = 10;
      end if;
      If(new.message like '%Slot%')
        Then
        SET var5 = ( SELECT grouping_parent_id from syslog where node_name = noname AND slot = SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' |Slot ',-1),' ',1) AND exec_time >= DATE_SUB(new.date_time, INTERVAL var6 SECOND) ORDER BY `syslog`.`id` DESC limit 1);
      else
        SET var5 = ( SELECT grouping_parent_id from syslog where node_name = noname AND exec_time >= DATE_SUB(new.date_time, INTERVAL var6 SECOND) ORDER BY `syslog`.`id` DESC limit 1);
      end if;
      -- check group line number
      SET gr_content_id = ( SELECT grouping_content_id from syslog where content = Content_Name AND exec_time >= DATE_SUB(new.date_time, INTERVAL var6 SECOND) ORDER BY `syslog`.`id` DESC limit 1);

      if(var4 like '%1%')
        then
        set var3 = 1;
      else
        -- insert
        insert into syslog (log, priority, exec_time, create_time, update_time, event_id, map_id, ip_addr,status, slot, node_name,port,place, device_name, node_id, content, client_name)
        values (SUBSTRING_INDEX(SUBSTRING_INDEX(new.message,' - ',-1),' |Slot ',1) , new.priority, new.date_time, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, new.event_id, new.map_id, new.agent_addr,'1', SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' |Slot ',-1),' ',1),noname, SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' |Port ',-1),' ',1), var1, var2, id_of_node, Content_Name, Client_Name);
        SET last_id = ( SELECT LAST_INSERT_ID());
        -- update group parent and group line number
        IF (var5 and ( SELECT exec_time from syslog where grouping_parent_id = var5 ORDER BY `syslog`.`id` ASC limit 1) >= DATE_SUB(new.date_time, INTERVAL var6 SECOND))
          Then
          UPDATE syslog SET grouping_parent_id = last_id where grouping_parent_id = var5 OR id = last_id;
        else
          UPDATE syslog SET grouping_parent_id = last_id where id = last_id;
        end if;
        IF ( (Content_Name IS NULL) OR (( SELECT exec_time from syslog where grouping_content_id = gr_content_id ORDER BY `syslog`.`id` ASC limit 1) < DATE_SUB(new.date_time, INTERVAL var6 SECOND)))
          Then
          UPDATE syslog SET grouping_content_id = last_id where id = last_id;
        else
          UPDATE syslog SET grouping_content_id = last_id where grouping_content_id = gr_content_id OR id = last_id;
        end if;
      end if;
    elseIf(new.message like ('% |Slot %'))
      then
      if (SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' |Slot ',-1),' ',1) != 1) 
        then
        SET id_of_node  = ( SELECT id from nodes Where node_id = NodeID AND slot = SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' |Slot ',-1),' ',1) ORDER BY `nodes`.`id` ASC limit 1);
        SET var1  = ( SELECT place from nodes Where node_id = NodeID AND slot = SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' |Slot ',-1),' ',1) ORDER BY `nodes`.`id` ASC limit 1);
        SET var2 = ( SELECT device_name from nodes Where node_id = NodeID AND slot = SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' |Slot ',-1),' ',1) ORDER BY `nodes`.`id` ASC limit 1);
        SET Content_Name  = (SELECT GROUP_CONCAT(DISTINCT `name` SEPARATOR '、') listing FROM
                      ( 
                        SELECT C.`name`
                        FROM `nodes_to_contents` N LEFT JOIN `contents` C ON C.`id` = N.`contents_id`  
                        Where N.`node_id` = id_of_node ) AA);

        SET Client_Name  = (SELECT CONCAT_WS('、',`to_client`,`name`) listing FROM
                      ( 
                        SELECT A.`name`, GROUP_CONCAT(B.`name` SEPARATOR '、') `to_client`
                        FROM `nodes_to_contents` N 
                        LEFT JOIN `contents` C ON C.`id` = N.`contents_id` 
                        LEFT JOIN `clients` A ON C.`contract_clients_id` = A.`id` 
                        LEFT JOIN `contents_to_client` D ON C.`id` = D.`contents_id` 
                        LEFT JOIN `clients` B ON D.`to_client_id` = B.`id` 
                        Where N.`node_id` = id_of_node ) AA);
        SET var4  = (Select inhibit from nodes where node_id = NodeID and slot = SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' |Slot ',-1),' ',1) ORDER BY `nodes`.`id` ASC limit 1 ); 
      
      else
        SET id_of_node  = ( SELECT id from nodes Where node_id = NodeID AND slot = SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' |Slot ',-1),' ',1) ORDER BY `nodes`.`id` ASC limit 1);
        SET var1  = ( SELECT place from nodes Where node_id = NodeID AND slot = SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' |Slot ',-1),' ',1) ORDER BY `nodes`.`id` ASC limit 1);
        SET var2 = ( SELECT device_name from nodes Where node_id = NodeID AND slot = SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' |Slot ',-1),' ',1) ORDER BY `nodes`.`id` ASC limit 1);
        SET Content_Name  = ( 
                          SELECT GROUP_CONCAT(DISTINCT C.`name` SEPARATOR '、') 
                          FROM `nodes` B 
                          LEFT JOIN `nodes_to_contents` N  ON B.`id` = N.`node_id`
                          LEFT JOIN `contents` C ON C.`id` = N.`contents_id`
                          Where B.`node_id` = NodeID );
        SET Client_Name  = (SELECT CONCAT_WS('、',`to_client`,`name`) listing FROM
                        ( 
                          SELECT GROUP_CONCAT(DISTINCT A.`name` SEPARATOR '、') `name`, GROUP_CONCAT(DISTINCT B.`name` SEPARATOR '、') `to_client`
                          FROM `nodes` N1 
                          LEFT JOIN `nodes_to_contents` N ON N1.`id` = N.`node_id` 
                          LEFT JOIN `contents` C ON C.`id` = N.`contents_id` 
                          LEFT JOIN `clients` A ON C.`contract_clients_id` = A.`id` 
                          LEFT JOIN `contents_to_client` D ON C.`id` = D.`contents_id` 
                          LEFT JOIN `clients` B ON D.`to_client_id` = B.`id`  
                          Where N1.`node_id` = NodeID ) AA);
        SET var4  = (Select inhibit from nodes where node_id = NodeID and slot = SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' |Slot ',-1),' ',1) ORDER BY `nodes`.`id` ASC limit 1 );
      end if;

      -- check group parent 
      SET var6 = ( SELECT grouping_interval from nodes where id = id_of_node ORDER BY id DESC limit 1);
      If (var6 IS NULL)
        THEN
        SET var6 = 10;
      end if;
      If(new.message like '%Slot%')
        Then
        SET var5 = ( SELECT grouping_parent_id from syslog where node_name = noname AND slot = SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' |Slot ',-1),' ',1) AND exec_time >= DATE_SUB(new.date_time, INTERVAL var6 SECOND) ORDER BY `syslog`.`id` DESC limit 1);
      else
        SET var5 = ( SELECT grouping_parent_id from syslog where node_name = noname AND exec_time >= DATE_SUB(new.date_time, INTERVAL var6 SECOND) ORDER BY `syslog`.`id` DESC limit 1);
      end if;
      -- check group line number
      SET gr_content_id = ( SELECT grouping_content_id from syslog where content = Content_Name AND exec_time >= DATE_SUB(new.date_time, INTERVAL var6 SECOND) ORDER BY `syslog`.`id` DESC limit 1);

      if(var4 like '%1%')
        then
        set var3 = 1;
      else
        -- insert
        insert into syslog (log, priority, exec_time, create_time, update_time, event_id, map_id, ip_addr,status, slot, node_name, place, device_name, node_id, content, client_name)
        values (SUBSTRING_INDEX(SUBSTRING_INDEX(new.message,' - ',-1),' |Slot ',1) , new.priority, new.date_time, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, new.event_id, new.map_id, new.agent_addr,'1', SUBSTRING_INDEX(SUBSTRING_INDEX(RTRIM(new.message),' |Slot ',-1),' ',1),noname, var1, var2, id_of_node, Content_Name, Client_Name);
        SET last_id = ( SELECT LAST_INSERT_ID());
        -- update group parent and group line number
        IF (var5 and ( SELECT exec_time from syslog where grouping_parent_id = var5 ORDER BY `syslog`.`id` ASC limit 1) >= DATE_SUB(new.date_time, INTERVAL var6 SECOND))
          Then
          UPDATE syslog SET grouping_parent_id = last_id where grouping_parent_id = var5 OR id = last_id;
        else
          UPDATE syslog SET grouping_parent_id = last_id where id = last_id;
        end if;
        IF ( (Content_Name IS NULL) OR (( SELECT exec_time from syslog where grouping_content_id = gr_content_id ORDER BY `syslog`.`id` ASC limit 1) < DATE_SUB(new.date_time, INTERVAL var6 SECOND)))
          Then
          UPDATE syslog SET grouping_content_id = last_id where id = last_id;
        else
          UPDATE syslog SET grouping_content_id = last_id where grouping_content_id = gr_content_id OR id = last_id;
        end if;
      end if;

    else
      SET var1  = ( SELECT GROUP_CONCAT(DISTINCT `place` SEPARATOR '、') from `nodes` Where `node_id` = NodeID);
      SET var2 = ( SELECT GROUP_CONCAT(DISTINCT `device_name` SEPARATOR '、') from `nodes` Where `node_id` = NodeID);
      SET Content_Name  = ( 
                          SELECT GROUP_CONCAT(DISTINCT C.`name` SEPARATOR '、') 
                          FROM `nodes` B 
                          LEFT JOIN `nodes_to_contents` N  ON B.`id` = N.`node_id`
                          LEFT JOIN `contents` C ON C.`id` = N.`contents_id`
                          Where B.`node_id` = NodeID );
      SET Client_Name  = (SELECT CONCAT_WS('、',`to_client`,`name`) listing FROM
                        ( 
                          SELECT GROUP_CONCAT(DISTINCT A.`name` SEPARATOR '、') `name`, GROUP_CONCAT(DISTINCT B.`name` SEPARATOR '、') `to_client`
                          FROM `nodes` N1 
                          LEFT JOIN `nodes_to_contents` N ON N1.`id` = N.`node_id` 
                          LEFT JOIN `contents` C ON C.`id` = N.`contents_id` 
                          LEFT JOIN `clients` A ON C.`contract_clients_id` = A.`id` 
                          LEFT JOIN `contents_to_client` D ON C.`id` = D.`contents_id` 
                          LEFT JOIN `clients` B ON D.`to_client_id` = B.`id`  
                          Where N1.`node_id` = NodeID ) AA);
      insert into syslog (log, priority, exec_time, create_time, update_time, event_id, map_id, ip_addr,status,node_name, place, device_name, node_id, content, client_name)
      values (SUBSTRING_INDEX(new.message,' - ',-1), new.priority, new.date_time, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, new.event_id, new.map_id, new.agent_addr,'1',noname, var1, var2, NULL, Content_Name, Client_Name);
      SET last_id = ( SELECT LAST_INSERT_ID());
      UPDATE syslog SET grouping_parent_id = last_id where id = last_id;
      UPDATE syslog SET grouping_content_id = last_id where id = last_id;
    end if;
  else
    SET var1  = ( SELECT place from nodes Where node_id = NodeID ORDER BY `nodes`.`id` ASC limit 1);
    SET var2 = ( SELECT device_name from nodes Where node_id = NodeID ORDER BY `nodes`.`id` ASC limit 1);
    SET id_of_node  = ( SELECT id from nodes Where node_id = NodeID ORDER BY `nodes`.`id` ASC limit 1);
    SET Content_Name  = (SELECT GROUP_CONCAT(DISTINCT `name` SEPARATOR '、') listing FROM
                      ( 
                        SELECT C.`name`
                        FROM `nodes_to_contents` N LEFT JOIN `contents` C ON C.`id` = N.`contents_id`  
                        Where N.`node_id` = id_of_node ) AA);

    SET Client_Name  = (SELECT CONCAT_WS('、',`to_client`,`name`) listing FROM
                      ( 
                        SELECT A.`name`, GROUP_CONCAT(B.`name` SEPARATOR '、') `to_client`
                        FROM `nodes_to_contents` N 
                        LEFT JOIN `contents` C ON C.`id` = N.`contents_id` 
                        LEFT JOIN `clients` A ON C.`contract_clients_id` = A.`id` 
                        LEFT JOIN `contents_to_client` D ON C.`id` = D.`contents_id` 
                        LEFT JOIN `clients` B ON D.`to_client_id` = B.`id` 
                        Where N.`node_id` = id_of_node ) AA);
    SET var4  = ( SELECT inhibit from nodes Where node_id = NodeID ORDER BY `nodes`.`id` ASC limit 1);
    -- check group parent 
    SET var6 = ( SELECT grouping_interval from nodes where id = id_of_node ORDER BY id DESC limit 1);
    If (var6 IS NULL)
      THEN
      SET var6 = 10;
    end if;
    SET var5 = ( SELECT grouping_parent_id from syslog where node_name = noname AND exec_time >= DATE_SUB(new.date_time, INTERVAL var6 SECOND) ORDER BY `syslog`.`id` DESC limit 1);
    -- check group line number
    SET gr_content_id = ( SELECT grouping_content_id from syslog where content = Content_Name AND exec_time >= DATE_SUB(new.date_time, INTERVAL var6 SECOND) ORDER BY `syslog`.`id` DESC limit 1);

    if(var4 like '%1%')
      then
      set var3 = 1;
    else
      if(new.message like  ('% |S %'))
        then
        set S = SUBSTRING_INDEX(SUBSTRING_INDEX(new.message,' |S ',-1),' ',1);
        set C = SUBSTRING_INDEX(SUBSTRING_INDEX(new.message,' |C ',-1),' ',1);
        set D = SUBSTRING_INDEX(SUBSTRING_INDEX(new.message,' |D ',-1),' ',1);
        
        if(D = 'release')
          then
          set prior = 7;
        else
          if(S = 'critical')
            then 
            set prior = 1;
          elseif(S = 'error ' OR S ='major' OR S = 'occur')
            then 
            set prior = 3;
          elseif(S = 'minor') 
            then 
            set prior = 4;
          elseif(S = 'warn ' or S = 'warning' or S = 'notice')
            then 
            set prior = 5;
          elseif(S = 'normal ')
            then 
            set prior = 6;
          elseif(S = 'cleared ' OR S = 'recover')  
            then 
            set prior = 7;                                   
          else
            set prior = 8;
          end if;
        end if;
        -- insert
        insert into syslog (log, priority, exec_time, create_time, update_time, event_id, map_id, ip_addr,status,  node_name,  place, device_name, node_id, content, client_name )
        values (SUBSTRING_INDEX(SUBSTRING_INDEX(new.message,' - ',-1),'|S',1),prior, new.date_time, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, new.event_id, new.map_id, new.agent_addr,'1',  noname, var1, var2, id_of_node, Content_Name, Client_Name);
        SET last_id = ( SELECT LAST_INSERT_ID());
        -- update group parent and group line number
        IF (var5 and ( SELECT exec_time from syslog where grouping_parent_id = var5 ORDER BY `syslog`.`id` ASC limit 1) >= DATE_SUB(new.date_time, INTERVAL var6 SECOND))
          Then
          UPDATE syslog SET grouping_parent_id = last_id where grouping_parent_id = var5 OR id = last_id;
        else
          UPDATE syslog SET grouping_parent_id = last_id where id = last_id;
        end if;
        IF ( (Content_Name IS NULL) OR (( SELECT exec_time from syslog where grouping_content_id = gr_content_id ORDER BY `syslog`.`id` ASC limit 1) < DATE_SUB(new.date_time, INTERVAL var6 SECOND)))
          Then
          UPDATE syslog SET grouping_content_id = last_id where id = last_id;
        else
          UPDATE syslog SET grouping_content_id = last_id where grouping_content_id = gr_content_id OR id = last_id;
        end if;
      else
        -- insert
        insert into syslog (log, priority, exec_time, create_time, update_time, event_id, map_id, ip_addr,status,  node_name,  place, device_name, node_id, content, client_name)
        values (SUBSTRING_INDEX(new.message,' - ',-1), new.priority, new.date_time, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, new.event_id, new.map_id, new.agent_addr,'1',  noname, var1, var2, id_of_node, Content_Name, Client_Name);
        SET last_id = ( SELECT LAST_INSERT_ID());
        -- update group parent and group line number
        IF (var5 and ( SELECT exec_time from syslog where grouping_parent_id = var5 ORDER BY `syslog`.`id` ASC limit 1) >= DATE_SUB(new.date_time, INTERVAL var6 SECOND))
          Then
          UPDATE syslog SET grouping_parent_id = last_id where grouping_parent_id = var5 OR id = last_id;
        else
          UPDATE syslog SET grouping_parent_id = last_id where id = last_id;
        end if;
        IF ( (Content_Name IS NULL) OR (( SELECT exec_time from syslog where grouping_content_id = gr_content_id ORDER BY `syslog`.`id` ASC limit 1) < DATE_SUB(new.date_time, INTERVAL var6 SECOND)))
          Then
          UPDATE syslog SET grouping_content_id = last_id where id = last_id;
        else
          UPDATE syslog SET grouping_content_id = last_id where grouping_content_id = gr_content_id OR id = last_id;
        end if;
      end if;
    end if;
  End If;
  if (afterAlivecondition != beforeAlivecondition and afterAlivecondition != '' and beforeAlivecondition != '')
    Then
    UPDATE `tra_nodes` LEFT JOIN `nodes` ON `nodes`.`id` = `tra_nodes`.`nodes_id` SET `tra_nodes`.`alive_condition` = afterAlivecondition, `tra_nodes`.`alive_condition_date`=CURRENT_TIMESTAMP Where `nodes`.`node_id` = NodeID;
  end if;
End IF;
-- end check message

end
$$
DELIMITER ;
